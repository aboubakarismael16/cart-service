FROM alpine
COPY . /cart-service
WORKDIR /cart-service
ENTRYPOINT [ "/cart-service" ]
